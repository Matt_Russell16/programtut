﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramTut
{
    class Program
    {

        static int age = 17;
        static void Main(string[] args)
        {

            // This is a comment
            Console.WriteLine("Hello World!");
            Console.WriteLine("I will print on a new line.");

            /* The code below will print the words Hello World
            to the screen, and it is amazing */

            Console.Write("Hello World! ");
            Console.Write("I will print on the same line. ");

           
            string firstName = "Matt ";
            string lastName = "Russell ";
            string fullName = firstName + lastName;
            Console.WriteLine();
            Console.WriteLine("Hello " + fullName + age);

            int myNum;
            myNum = 16;
            Console.WriteLine(myNum);

            int myNum2 = 17;
            string message = ("Your new number is now ");
            string message2 = (".");
            myNum2 = 17; // myNum2 is now 17
            Console.WriteLine(message + myNum2 + message2);


            string message3 = ("Congratulations you have successfully worked out what x * y is. ");
            int x = 5;
            int y = 6;
            Console.WriteLine(x * y); //Prints the value of x + y
            Console.WriteLine(message3);

            int x2 = 5, y2 = 6, z = 5;
            Console.WriteLine(x2 + y2 + z);
            Console.WriteLine("Congratulations you have worked out your age in one whole sum");

            Console.ReadKey();

         

        }
    }
}
